/*
 *	core.cpp
 *		Author: SegFaultLyfe (Toby Wong)
 */

#include <iostream>
#include <limits>

#include "core.hpp"
#include "board.hpp"
#include "occupant.hpp"
#include "tile.hpp"

namespace TDRPG{
    CORE::CORE(){
        InputLoop:
        std::cout << "Please enter board size larger than 4: ";
        if (!(std::cin >> this->board_size)){
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            goto InputLoop;
        }
        if (this->board_size < 3){
            std::cin.clear();
            goto InputLoop;
        }
        board_object.initialise(board_size);
        current_player.setHP(10);
    }

    void CORE::gameProcess(){
        current_x = 0, current_y = board_size - 1;
        board_object.generateBoard();
        while (game_over_flag != 1){
            
            screen_washer(5);
            board_object.printBoard();  
            screen_washer(3);

            std::cout << "Your HP is: " << current_player.getHP() << std::endl;
            std::cout << "P = Player, □ = Enemy, ● = Hills\n↑ = 1, ↓ = 2, ← = 3, → = 4";
            //User Directions Input Prompt
            Direction_Loop:
            std::cout << "Please Enter your Directions: ";

            //Check if Player Alive
            if (current_player.getHP() <= 0){
                game_over_flag = 1;
                game_over();
            }
            if (!(std::cin >> this->current_control)){
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                goto Direction_Loop;
            }
            if ((this->current_control > 4)&&(this->current_control < 1)){
                std::cin.clear();
                goto Direction_Loop;
            }
            if (checkValidity(current_x, current_y, current_control) != true){
                goto Direction_Loop;
            }
            //Check if Player Alive
            if (current_player.getHP() < 0){
                game_over_flag = 1;
                game_over();
            }
            
        }
    }

    void CORE::player_combat(int cur_x, int cur_y, int dest_x, int dest_y){
        if (board_object.board_template[dest_y][dest_x].getSpawnType() == OccupantType::ENEMY){
            current_player.damage(3);
            board_object.board_template[dest_y][dest_x].getOccupant().die();
            std::cout << "The enemy dealt you 3 damage!\n";
            if (current_player.getHP() == 0){
                game_over_flag = 1;
                game_over();
            } else {
                std::cout << "You are rewarded 5 HP for slaying the enemy!\n";
                current_player.damage(-5);
                board_object.board_template[dest_y][dest_x].setOccupant(current_player);
                board_object.board_template[dest_y][dest_x].setSpawnType(OccupantType::PLAYER);
                board_object.board_template[cur_y][cur_x].setSpawnType(OccupantType::EMPTY);

                current_x = dest_x;
                current_y = dest_y;
            }
        } else if (board_object.board_template[dest_y][dest_x].getSpawnType() == OccupantType::BOSS){
            std::cout << "Boss Ricardo the Dancer dealt you 20 damage! Ouch!\n";
            current_player.damage(2 * board_size);
            if (current_player.getHP() == 0){
                game_over_flag = 1;
                game_over();
            } else {
                board_object.cleanBoard();
                board_object.generateBoard();
                current_x = 0;
                current_y = board_size -1;
            }
        } else {
            board_object.board_template[dest_y][dest_x].setOccupant(current_player);
            board_object.board_template[dest_y][dest_x].setSpawnType(OccupantType::PLAYER);
            board_object.board_template[cur_y][cur_x].setSpawnType(OccupantType::EMPTY);

            current_x = dest_x;
            current_y = dest_y;
        }

    }


    bool CORE::checkValidity(int x, int y, int directions){
        int temp_x = x, temp_y = y;

        //Checking user direction is valid or not
        if (directions == 1){

            //If Direction is UP
            temp_y -= 1;
            if ((temp_y < 0) || (temp_y > board_size - 1)){
                std::cout << "You almost fell off the universe! -1HP\n";
                current_player.damage(1);
                return false;
            } else if (board_object.board_template[temp_y][current_x].isWalled() == true){
                std::cout << "Blimey Blimey o'Rhiley, your head was hit by a rock falling from the hills! -1HP\n";
                current_player.damage(1);
                return false;
            } else {
                player_combat(current_x, current_y, temp_x, temp_y);
                return true;
            }


        } else if (directions == 2){

            //If Direction is DOWN
            temp_y += 1;
            if ((temp_y < 0) || (temp_y > board_size - 1)){
                std::cout << "You almost fell off the universe! -1HP\n";
                current_player.damage(1);
                return false;
            } else if (board_object.board_template[temp_y][current_x].isWalled() == true){
                std::cout << "Blimey Blimey o'Rhiley, your head was hit by a rock falling from the hills! -1HP\n";
                current_player.damage(1);
                return false;
            } else {
                player_combat(current_x, current_y, temp_x, temp_y);
                return true;
            }


        } else if (directions == 3){

            //If Direction is LEFT
            temp_x -= 1;
            if ((temp_x < 0) || (temp_x > board_size - 1)){
                std::cout << "You almost fell off the universe! -1HP\n";
                current_player.damage(1);
                return false;
            } else if (board_object.board_template[y][temp_x].isWalled() == true){
                std::cout << "Blimey Blimey o'Rhiley, your head was hit by a rock falling from the hills! -1HP\n";
                current_player.damage(1);
                return false;
            } else {
                player_combat(current_x, current_y, temp_x, temp_y);
                return true;
            }


        } else if (directions == 4){

            //If Direction is RIGHT
            temp_x += 1;
            if ((temp_x < 0) || (temp_x > board_size - 1)){
                std::cout << "You almost fell off the universe! -1HP\n";
                current_player.damage(1);
                return false;
            } else if (board_object.board_template[current_y][temp_x].isWalled() == true){
                std::cout << "Blimey Blimey o'Rhiley, your head was hit by a rock falling from the hills! -1HP\n";
                current_player.damage(1);
                return false;
            } else {
                player_combat(current_x, current_y, temp_x, temp_y);
                return true;
            }

        }
    }

    void CORE::game_over(){
        screen_washer(20);
        std::cout << "You died, game over.\n"; 
        screen_washer(10);
    }
    
    void CORE::screen_washer(int amt){
        for (int i = 0; i < amt; i++){
            std::cout << std::endl;
        }
    }
}