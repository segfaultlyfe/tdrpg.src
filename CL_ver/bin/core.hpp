/*
 *	core.hpp
 *		Author: SegFaultLyfe (Toby Wong)
 */

#ifndef CORE_HPP_
#define CORE_HPP_

#include "iostream"
#include "board.hpp"
#include "occupant.hpp"
#include "tile.hpp"

namespace TDRPG { 
    class CORE {
    public:
        //Constructor initialises user input
        CORE();
        
        //Checks if destination cell locatable
        bool checkValidity(int x, int y, int directions);

        //Game Logic CoreCore
        void gameProcess();

        //Player versus enemy
        void player_combat(int x, int y, int dx, int dy);

        //Ricardo's Screen Washing Co.
        void screen_washer(int amount);

        void game_over();

    private:
        Board board_object;
        Occupant current_player;
        int board_size, game_over_flag = 0, current_control, current_x, current_y;
    };
}

#endif // !CORE_HPP_