#include <iostream>

#include "core.hpp"

int main(){
    TDRPG::CORE *x = new TDRPG::CORE();
    x->gameProcess();
    delete x;
}