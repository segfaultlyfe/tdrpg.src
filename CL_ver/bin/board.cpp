/*
 *	board.cpp
 *		Author: SegFaultLyfe (Toby Wong, Will Christensen, Jake Gianola)
 */

#include <algorithm>
#include <iostream>
#include <vector>
#include <random>

#include "board.hpp"
#include "occupant.hpp"
#include "tile.hpp"

namespace TDRPG{
    Board::Board(){
    }

    void Board::initialise(int sz){
        board_size = sz;
        board_template.resize(board_size, std::vector<Tile>(board_size));
    }

    void Board::generateBoard(){
        //Set Default Player Starting Point and Boss Location
        board_template[board_size-1][0].setSpawnType(OccupantType::PLAYER);
        board_template[board_size-1][0].setTouched(true);
        board_template[0][board_size-1].setSpawnType(OccupantType::BOSS);
        board_template[0][board_size-1].setTouched(true);
        
        //Random Seed
        std::random_device gBrn;

        //Random Engines
        std::default_random_engine engine_Occupant(gBrn());
        std::default_random_engine engine_Terrain(gBrn());
        std::default_random_engine engine_Enemies(gBrn());
        std::default_random_engine engine_xAx(gBrn());
        std::default_random_engine engine_yAx(gBrn());

        //Random Distrubutions
        std::uniform_int_distribution<int> distru_Occupant(0, 4);
        std::uniform_int_distribution<int> distru_Terrain(0, ((board_size)*(board_size)/3));
        std::uniform_int_distribution<int> distru_Enemies(0, ((board_size)*(board_size)/3));
        std::uniform_int_distribution<int> distru_xAx(0, board_size-1);
        std::uniform_int_distribution<int> distru_yAx(0, board_size-1);

        //Generate a Path going to be used as basis
        generatePath();

        //Amount of preferred walls/enemies
        int Terrain = distru_Terrain(engine_Terrain);
        int Enemies = distru_Enemies(engine_Enemies);

        //Set Terrain
        for (int i = 1; i <= Terrain; i++){
            Terrain_Mapping:
            int x = distru_xAx(engine_xAx);
            int y = distru_yAx(engine_yAx);
            if (board_template[y][x].isTouched() == false){
                board_template[y][y].setTouched(true);
                board_template[y][x].setWalled(true);
            } else {
                goto Terrain_Mapping;
            }
        }

        //Set Enemies
        for (int i = 1; i <= Enemies; i++){
            Enemies_Mapping:
            int x = distru_xAx(engine_xAx);
            int y = distru_yAx(engine_yAx);
            if ((board_template[y][x].isWalled() == false)&&(board_template[y][x].getSpawnType() != OccupantType::BOSS)&&(board_template[y][x].getSpawnType() != OccupantType::PLAYER)){
                board_template[y][y].setTouched(true);
                board_template[y][x].setSpawnType(OccupantType::ENEMY);
            } else {
                goto Enemies_Mapping;
            }
        }
    }

    void Board::generatePath(){
        int FLAG = 0, top_bound = 0, right_bound = 0, i, x = 0, y = board_size-1;

        //Random Seed
        std::random_device gPrn;

        //Random Engine
        std::default_random_engine engine_2direct(gPrn());

        //Random Distribution
        std::uniform_int_distribution<int> distru_2direct(0, 1);

        while (FLAG != 1){
            int Direction = distru_2direct(engine_2direct);

            //IMMIGRATION
            if (x == board_size-1){
                right_bound = 1;
            } else if (y == 0){
                top_bound = 1;
            }
            //If reached borderline
            if (top_bound == 1){
                for (std::vector<Tile>::iterator tb_x = board_template[0].begin() + x + 1; tb_x != board_template[0].end(); tb_x++){
                    x += 1;
                    tb_x->setTouched(true);
                }
            } else if (right_bound == 1){
                for (y; y > 0; y--){
                    board_template[y][board_size-1].setTouched(true);
                }
            } else {
                //Normal Random Directions
                if (Direction == 0){
                    y -= 1;
                    board_template[y][x].setTouched(true);
                } else if (Direction == 1){
                    x += 1;
                    board_template[y][x].setTouched(true);
                }
            }

            if (board_template[y][x].getSpawnType() == OccupantType::BOSS){
                FLAG = 1;
            }
        }
    }

    void Board::cleanBoard(){
        for (std::vector<std::vector<Tile>>::iterator i = board_template.begin(); i != board_template.end(); i++){
            for (std::vector<Tile>::iterator j = i->begin(); j != i->end(); j++){
                j->setSpawnType(OccupantType::EMPTY);
                j->setWalled(false);
                j->setTouched(false);
            }
        }
    }

    void Board::printBoard(){
        for (std::vector<std::vector<Tile>>::iterator i = board_template.begin(); i != board_template.end(); i++){
            for (std::vector<Tile>::iterator j = i->begin(); j != i->end(); j++){
                if (j->getSpawnType() == OccupantType::PLAYER){
                    std::cout << "P ";
                } else if (j->getSpawnType() == OccupantType::ENEMY) {
                    std::cout << "□ ";
                } else if (j->getSpawnType() == OccupantType::BOSS) {
                    std::cout << "B ";
                } else if (j->isWalled() == true){
                    std::cout << "● ";
                } else {
                    std::cout << "  ";
                }
            }
            std::cout << std::endl;
        }
    }

    int Board::getSize(){
        return board_size;
    }

    Tile& Board::getTile(int x, int y) {
		return board_template[y][x];
	}

    bool Board::swapTiles(int x1, int y1, int x2, int y2) {
        if (x1 < 0 || x2 < 0 || y1 < 0 || y2 < 0 || x1 >= board_size || x2 >= board_size || y1 >= board_size || y2>= board_size) {
            return false;
        }
        else if (board_template[y1][x1].isWalled() || board_template[y2][x2].isWalled()) {
            return false;
        }
        else {
        Tile temp = board_template[y2][x2];
        board_template[y2][x2] = board_template[y1][x1];
        board_template[y1][x1] = temp;
        return true;
        }
    }

    Board::~Board() {
        board_size = 0; //Placeholder line
    }
}
