/*
 *	occupant.cpp
 *		Author: SegFaultLyfe (Toby Wong, Joseph Goh, ADD YOUR NAME IF YOU WORK ON IT HERE)
 */

#include "occupant.hpp"

namespace TDRPG{
    void Occupant::setType(OccupantType X){
        type = X;
    }

    void Occupant::setHP(int new_hp){
		if (new_hp < 0) {
			HP = 0;
		}
		else {
			HP = new_hp;
		}
    }

    void Occupant::setATK(int new_atk){
        ATK = new_atk;
    }

    void Occupant::setDEF(int new_def){
        DEF = new_def;
    }

    void Occupant::die(){
        HP = 0;
        ATK = 0;
        DEF = 0;
    }

    void Occupant::damage(int amt){
        HP -= amt;
    }

    int Occupant::getHP(){
        return HP;
    }

    int Occupant::getATK(){
        return ATK;
    }

    int Occupant::getDEF(){
        return DEF;
    }

    OccupantType Occupant::getType(){
        return type;
    }
}
