/*
 *	player.cpp
 *		Author: SegFaultLyfe (Jake Gianola, Joseph Goh, ADD YOUR NAME IF YOU WORK ON IT HERE)
 */

#include "player.hpp"

namespace TDRPG {
    Player::Player() {
        this->type = OccupantType::PLAYER;
        this->HP = 10;
        this->ATK = 4;
        this->DEF = 5;
    }

	bool Player::isAlive() {
		return alive;
	}

	void Player::die() {
		alive = false;
	}
}
