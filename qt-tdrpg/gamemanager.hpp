/*
*	gamemanager.hpp
*		Author: SegFaultLyfe (Joseph Goh, ADD YOUR NAME IF YOU WORK ON IT HERE)
*/

#ifndef GAMEMANAGER_HPP_
#define GAMEMANAGER_HPP_

#include <vector>

#include <Qt>
#include <QWidget>
#include <QKeyEvent>
#include <QGridLayout>

#include "board.hpp"
#include "mainwindow.hpp"
#include "playercontroller.hpp"
#include "tilegraphic.hpp"

namespace TDRPG {
	class GameManager : public QWidget {
	public:
        // Initialize entire game
        GameManager(int sz);
        // releaseKeyboard
        ~GameManager();

        QGridLayout* getGameDisplay();

        void keyPressEvent(QKeyEvent *event);
        void gameover();
        void newLevel();
        void updateGraphics();
	protected:
        Board board;
        Player player;
        PlayerController controller;
        int level = 0;
        std::vector<std::vector<Tilegraphic*>> tilegraphics;
        QGridLayout gamedisplay;
	};
}

#endif // GAMEMANAGER_HPP_
