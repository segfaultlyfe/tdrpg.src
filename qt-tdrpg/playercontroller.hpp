/*
 *	playercontroller.hpp
 *		Author: SegFaultLyfe (Joseph Goh, Jake Gianola *add your name!*)
 */

#ifndef PLAYERCONTROLLER_H
#define PLAYERCONTROLLER_H

#include "player.hpp"
#include "board.hpp"

namespace TDRPG {
    class PlayerController {
    public:
        PlayerController(Player plyr);
        // Interact with Tile at relative location, the default interaction with an empty unwalled tile being to move there
        void interact(Board brd, int dx, int dy);
        // Move to Tile at relative location by calling Board::swapTiles
        bool move(Board brd, int dx, int dy);

        int getX();
        int getY();
        void setX(int new_x);
        void setY(int new_y);

    private:

		Player player;
		int cur_x;
		int cur_y;
        int score;

    };
}


#endif // PLAYERCONTROLLER_H
