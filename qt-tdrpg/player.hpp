/*
 *	player.hpp
 *		Author: SegFaultLyfe (Jake Gianola, ADD YOUR NAME IF YOU WORK ON IT HERE)
 */
#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include "occupant.hpp"

namespace TDRPG {
    class Player : public Occupant {
    public:
        Player();
        // Initialize type to OccupantType::PLAYER
        // Initialize HP to 10
        // Initialize ATK to 4
        // Initialize DEF to 5

		// Getter method for alive status
		bool isAlive();
		// Set alive status to false
		void die();
	protected:
		bool alive = true;
    };
}
#endif // PLAYER
