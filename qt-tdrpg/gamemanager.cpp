/*
*	gamemanager.cpp
*		Author: SegFaultLyfe (Joseph Goh, ADD YOUR NAME IF YOU WORK ON IT HERE)
*/

#include <iostream>

#include "gamemanager.hpp"
#include "enemy.hpp"
#include "boss.hpp"

namespace TDRPG {
    GameManager::GameManager(int sz) : board(sz), player(), controller(player), gamedisplay(){
        tilegraphics.resize(board.getSize(), std::vector<Tilegraphic*>(board.getSize()));
        for (int row = 0; row < sz; row++) {
            for (int col = 0; col < sz; col++) {
                tilegraphics[row][col] = new Tilegraphic();
            }
        }
        for (int x = 0; x < board.getSize(); ++x) {
            for (int y = 0; y < board.getSize(); ++y) {
                gamedisplay.addWidget(tilegraphics[y][x], y, x);
            }
        }
        newLevel();
        grabKeyboard();
	}

    GameManager::~GameManager() {
        for (int row = 0; row < board.getSize(); row++) {
            for (int col = 0; col < board.getSize(); col++) {
                delete tilegraphics[row][col];
            }
        }
        releaseKeyboard();
    }

    QGridLayout* GameManager::getGameDisplay() {
        return &gamedisplay;
    }

	void GameManager::keyPressEvent(QKeyEvent *event) {
        int key = event->key();
        if (key == Qt::Key_Left) {
            controller.interact(board, -1, 0);
		}
		else if (key == Qt::Key_Right) {
            controller.interact(board, 1, 0);
		}
		else if (key == Qt::Key_Up) {
            controller.interact(board, 0, -1);
		}
		else if (key == Qt::Key_Down) {
            controller.interact(board, 0, 1);
		}
        else if (key == Qt::Key_Backspace) {
            player.die();
        }

        // If the player is dead it's gameover
        if (!player.isAlive()) {
            gameover();
        }
        // If the player has reached the upper right corner it's a whole new world
        else if (controller.getX() == board.getSize() - 1 && controller.getY() == 0) {
            newLevel();
        }

        // Always update graphics after keypress
        updateGraphics();

        QWidget::keyPressEvent(event);
	}

    void GameManager::newLevel() {
        board.cleanBoard();
        board.generateBoard();
        // Iterate through each tile and spawn appropriate occupants
        for (int x = 0; x < board.getSize(); ++x) {
            for (int y = 0; y < board.getSize(); ++y) {
                if (board.getTile(x, y).getSpawnType() == OccupantType::PLAYER) {
                    controller.setX(x);
                    controller.setY(y);
                    board.getTile(x, y).setOccupant(player);
                }
                else if (board.getTile(x, y).getSpawnType() == OccupantType::ENEMY) {
                    board.getTile(x, y).setOccupant(Enemy(4 + level, 6 + level, 1 + level));
                }
                else if (board.getTile(x, y).getSpawnType() == OccupantType::BOSS) {
                    board.getTile(x, y).setOccupant(Boss());
                }

                tilegraphics[y][x]->update(board.getTile(x, y));
            }
        }
        level++;
    }

    void GameManager::updateGraphics() {
        for (int x = 0; x < board.getSize(); ++x) {
            for (int y = 0; y < board.getSize(); ++y) {
                tilegraphics[y][x]->update(board.getTile(x, y));
            }
        }
    }

    void GameManager::gameover() {
        // TODO: implement this
    }
}
