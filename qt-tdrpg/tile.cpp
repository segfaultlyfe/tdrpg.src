/*
 *	tile.cpp
 *		Author: SegFaultLyfe (Joseph Goh, Toby Wong, ADD YOUR NAME IF YOU WORK ON IT HERE)
 */

#include <stdexcept>

#include "tile.hpp"
#include "enemy.hpp"

namespace TDRPG{
    Tile::Tile(){}

    /*
    Tile::Tile(Tile& copytile){
        this->touched = copytile.touched;
        this->walled = copytile.walled;
        this->occupied = copytile.occupied;
        this->spawn_type = copytile.spawn_type;
        this->cur_occupant = copytile.cur_occupant;
    }
    */

    Occupant Tile::getOccupant() {
		if (isOccupied()) {
            return cur_occupant;
		}
		else {
			throw std::logic_error("Tried to access Occupant from unoccupied Tile");
		}
    }

    OccupantType Tile::getSpawnType(){
        return spawn_type;
	}

	bool Tile::isTouched(){
		return touched;
	}

	bool Tile::isWalled() {
		return walled;
	}

	bool Tile::isOccupied() {
		return occupied;
	}

    void Tile::setOccupant(Occupant occup) {
        cur_occupant = occup;
		occupied = true;
    }

    void Tile::setSpawnType(OccupantType type){
        spawn_type = type;
	}

	void Tile::setTouched(bool status){
		touched = status;
	}

	void Tile::setWalled(bool status) {
		walled = status;
	}

	void Tile::vacate() {
		occupied = false;
	}
}
