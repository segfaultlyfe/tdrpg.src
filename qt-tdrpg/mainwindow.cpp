/*
 *	mainwindow.cpp
 *		Author: SegFaultLyfe (Joseph Goh, Toby Wong, Will Christensen)
 */
#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "gamemanager.hpp"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{

}
