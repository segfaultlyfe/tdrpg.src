/*
 *	main.cpp
 *		Author: SegFaultLyfe (Joseph Goh, Toby Wong, Will Christensen)
 */

#include <QApplication>
#include <QDialog>

#include "mainwindow.hpp"
#include "tilegraphic.hpp"
#include "gamemanager.hpp"

using namespace TDRPG;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    QDialog d;

    GameManager gamemanager(5);

    d.setLayout(gamemanager.getGameDisplay());
    d.show();

    return a.exec();
}
