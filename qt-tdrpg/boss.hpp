/*
 *	boss.hpp
 *		Author: SegFaultLyfe (Toby Wong, ADD YOUR NAME IF YOU WORK ON IT HERE)
 */

#ifndef BOSS_HPP_
#define BOSS_HPP_

#include "enemy.hpp"

namespace TDRPG {
    class Boss : public Enemy {
	public:
		Boss();
        Boss(int hp, int atk, int def);
	};
}

#endif // !ENEMY_HPP_
