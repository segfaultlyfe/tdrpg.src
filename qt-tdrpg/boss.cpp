/*
 *	boss.cpp
 *		Author: SegFaultLyfe (Toby Wong, ADD YOUR NAME IF YOU WORK ON IT HERE)
 */

#include "boss.hpp"

namespace TDRPG{
    Boss::Boss(){
        type = OccupantType::BOSS;
        HP = 50;
        ATK = 10;
        DEF = 10;
    }

    Boss::Boss(int hp, int atk, int def) {
        HP = hp;
        ATK = atk;
        DEF = def;
    }
}
