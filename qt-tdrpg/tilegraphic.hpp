//Created by Jake Gianola

#ifndef TILEGRAPHIC_HPP
#define TILEGRAPHIC_HPP

#include <QLabel>
#include <QPixmap>

#include "tile.hpp"
#include "occupant.hpp"

namespace TDRPG {
    class Tilegraphic: public QLabel {
        Q_OBJECT
    public:
        Tilegraphic(QWidget *parent = nullptr);
        Tilegraphic(Tilegraphic& tile);
        void update(Tile tile);
     };
}
#endif // TILEGRAPHIC_HPP
