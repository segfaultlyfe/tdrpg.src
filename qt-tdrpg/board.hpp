/*
 *	board.hpp
 *		Author: SegFaultLyfe (Joseph Goh, Toby Wong, Will Christensen)
 */

#ifndef BOARD_HPP_
#define BOARD_HPP_

#include <vector>

#include "tile.hpp"

namespace TDRPG {
    // Board will contain a 2d vector of Tiles
    class Board {
    public:
        //Constructor on User Input, initialises 2-D vector Array
        Board(int sz);

        //Generate the board with other class elements
        void generateBoard();

        //Print board in command line, Debug use
        void printBoard();

        // Cleans the Board for next level
        void cleanBoard();

        //Getter for board size
        int getSize();

        // Getter for tile at specified location
        Tile& getTile(int x, int y);

        //Swap two tiles to move occupant (return success/failure as bool)
        bool swapTiles(int x1, int y1, int x2, int y2);

        // Destructor to reset board size and/or end current game state
        ~Board();

    private:
        int board_size;
        std::vector<std::vector<Tile>> board_template;

        //Generating a Single Random Passible Path
        void generatePath();
    };
}

#endif // !BOARD_HPP_
