2019/02/12
First group meeting was cancelled due to unexpected schedule conflicts

2019/02/14
Actual first group meeting happened with progress made such as:
	a. Set up project repository on BitBucket
	b. Created team Slack for future communications
	c. Decided to use Qt primarily for the project game
	d. Discussed the style, scope, and mechanics of the game (with no actual conclusions)
	e. Told Toby to go install a virtual machine (because we decided we're running and testing everything on Arch Linux)

More detailed paragraph analysis:
	On Thursday, February 14th for one hour, all four group members were able to meet and discuss 
preliminary aspects of our top down role playing game. The first order of business was 
to finally create our repository, we accomplished this and sent it out to you, and later
gave the gtfs the link as well to access. We then got to discussing our main modes of 
communication while developing the project, and we all eventually converged on using 
Slack over any other form of communication (if you need the link to that as well we
are more than happy to provide it). Once this was established, we begun to work to see 
exactly what libraries and third party software we would focus on using for the project.
To keep things simple, we're going to attempt to utilize Qt alone for the project, and 
if this endeavour turns for the worst we'll start to incorporate other libraries and 
software to help alieviate any problem that is unsolvable with Qt. Once this was set in 
place, we briefly went over game mechanics. What we landed on is not yet definitive, but 
we'll be working on a framework for just about everything during week 7 and dividing out 
specific roles based on what needs to be accomplished in that framework. And yes, we did 
tell Toby to install a virtual machine, as we all decided that testing our game exclusively
through a linux virtual box and ix-dev is the safest way to ensure that our game works at
each stage of development.

2019/02/17
Realized that the week 6 progress note deadline is today and wrote a quick rough draft
	a. (and also trembled in fear at the fact that it's already week 7)